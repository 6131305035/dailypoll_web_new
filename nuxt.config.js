import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  ssr : false,
  server : {
    port: 8210, // default: 3000,
    host : 'selab.mfu.ac.th',
  },
  head: {
    titleTemplate: '%s - dailypoll_new_app',
    title: 'dailypoll_new_app',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Kanit',  }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],

  pwa : {
    workbox : {
      enabled : true
    },
    manifest: {
      name: 'Nuxt.js PWA survival store',
      short_name: 'Nuxt.js PWA',
      lang: 'en',
      display: 'standalone',
    }
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  router : {
    extendRoutes(routes, resolve) {
      routes.push({
        path : '/Mmain',
        name: 'Admin',
        component: resolve(__dirname, 'pages/Mmain.vue')
      })
    }
  },

  env: {
    api_getPoll : process.env.getPollPORT || 'http://selab.mfu.ac.th:8310/api/v1/getPollDetail_Table',
    api_postPoll : process.env.postPollPORT || "http://selab.mfu.ac.th:8310/api/v1/postQuestion_Table/",
    
    api_getAdminMng : process.env.getAdminMngPORT || 'http://selab.mfu.ac.th:8310/api/v1/getAdminMng_Table',
    api_postAdminMng : process.env.postAdminMngPORT || 'http://selab.mfu.ac.th:8310/api/v1/postAdminMng_Table/',
    api_putAdminMng : process.env.putAdminMngPORT || 'http://selab.mfu.ac.th:8310/api/v1/updateAdminMng_Table/',
    api_deleteAdminMng : process.env.deleteAdminMngPORT || 'http://selab.mfu.ac.th:8310/api/v1/deleteAdminMng_Table/',
    
    api_getQuestion : process.env.getQuestionPORT || 'http://selab.mfu.ac.th:8310/api/v1/getQuestion_Table',

    googlemap_apikey : process.env.GoogleMap_ApiKey || "AIzaSyBtZEGeIIQhk1FYS6N4kPYDIJDe_MIrV9A"
  }
}
