module.exports = {
    apps : [
            {
                name : 'dailypoll_web_new',
                port : 8210,
                script : './node_modules/nuxt/bin/nuxt.js',
                cwd : '/home/spdailypoll/dailypoll_web_new',
                args : 'start',
                env: {
                    NODE_ENV: 'development'
                },
                env_production: {
                    NODE_ENV: 'production'
                }
        }
    ]
}